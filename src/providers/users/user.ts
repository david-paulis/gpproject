
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response ,Headers } from '@angular/http';
import { baseURL } from '../../shared/baseurl';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {User} from '../../shared/user';
import { Storage } from '@ionic/storage';
import {AuthDataProvider}  from '../../providers/auth-data/auth-data';
import {HttpProcessProvider} from "../http-process/http-process";
@Injectable()
export class Users {
  user: User;
  token:string;
  response:Response;
  constructor(public http: Http,private storage: Storage,private authService:AuthDataProvider,
      private processHTTPMsgService:HttpProcessProvider) {
        this.storage.get("auth").then(token=>{
            if(token)
            {
             this.token=token;
            
            }
        }).then(err=>{console.log(err)});
       }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any):Observable<User> {
     var headers= new Headers();
     if(this.token){
         headers.append("Authorization",this.token)
     }
     headers.append("Content-Type","application/json");
   return this.http.post(baseURL+"auth/",accountInfo,{headers:headers })
                   .map(res => { 
                      if(res.headers.has("Authorization"))
                      {
                       this.token=res.headers.get("Authorization");
                      }
                     
                     if(this.token)
                     {   this.authService.setToken(this.token);
                         this.storage.set("auth",this.token).then((token)=>{
                         console.log("token saved correctly",token);
                         });   
                     }
                    return this.processHTTPMsgService.extractData(res); })
                   .catch(
                       (error) =>{ 
                      
                        var status=error.status;
                        if(status ==401)
                        {
                            return Observable.throw("Incorrect Username Or Password ");
                        }
                        else 
                        {
                            return this.processHTTPMsgService.handleError(error);
                        }
                       
                    }
                    );
    
  }
 
  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
signup(user: any) {
     
      var headers= new Headers();
      headers.append("Content-Type","application/json");
      
      return this.http.post(baseURL+"auth/register/",user,{headers:headers })
                      .map(res =>{

                          var token =res.headers.get("Authorization");
                          this.authService.setToken(token);
                         if(token){this.storage.set("auth",token); }
                                               
                          return this.processHTTPMsgService.extractData(res);})
                      .catch(error =>{
                          this.response=error;
                          var status=this.response.status;
                          console.log("resStatus ",status);
                          if(status == 401){
                              return Observable.throw("Username or Email Already Used Choose Another One .")
                          }
                        return this.processHTTPMsgService.handleError(error)});
    
  }


}
