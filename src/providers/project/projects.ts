import { Injectable } from '@angular/core';
import {Project} from '../../shared/project';
import { Observable } from 'rxjs/Observable';
import { Http, Response ,Headers} from '@angular/http';
import { baseURL } from '../../shared/baseurl';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import 'rxjs/add/operator/catch';
import { Storage } from '@ionic/storage';
import {HttpProcessProvider} from "../http-process/http-process";
import { concat } from 'rxjs/operator/concat';
import {AuthDataProvider} from '../../providers/auth-data/auth-data';
 
@Injectable()
export class ProjectsProvider{
 token:string;
 errMsg:string;
 id:string;
 response:Response;
  constructor(private http:Http,private httpProcessProvider:HttpProcessProvider,
              private storage:Storage , private authService:AuthDataProvider) { 
  
            this.authService.getToken().subscribe(token => this.token=token);
             this.authService.getId().subscribe(id => this.id=id);
              }  

  getAllProject():Observable<Project[]>{
    
/*this.storage.get("auth").then((token)=>{
     this.token=token;
     this.storage.get("id").then((id)=>{
      this.id=id;
     });
   });
   */
      var headers= new Headers();
      headers.append("Content-Type","application/json");
      
      headers.append("Authorization",this.token);
      headers.append("User",this.id);

     return  this.http.get(baseURL+"project/",{headers:headers})
      .map(res=>{   
        return this.httpProcessProvider.extractData(res);})
      .catch(
        error =>{ 
          this.response=error;
          var status =this.response.status;
          console.log("resStatus ",status); 
          if(status == 400)
          {
            return Observable.throw("Invalid Access Please Login Again");
             
          }
          else 
          {
            return this.httpProcessProvider.handleError(error);
          }
        }

      ); 

}
 

  addProject(project:Project):Observable<Project> {
      var headers= new Headers();
      headers.append("Content-Type","application/json");
      headers.append("Authorization",this.token);
      headers.append("User",this.id);

    return  this.http.post(baseURL+"project/",project,{headers:headers})
                .map(res=>{
                  this.errMsg=res.headers.get("detail")
                  return this.httpProcessProvider.extractData(res);})
                .catch(error =>{ 
                  this.response=error;
                  var status =this.response.status;
                  console.log("resStatus ",status);
                  if(status == 400)
                  {
                    return Observable.throw("Invalid Access Please Login Again");
                     
                  }
                  else 
                  {
                    return this.httpProcessProvider.handleError(error);
                  }
                  });

  }
  removeProject(id:number){
            var headers= new Headers();
               headers.append("Content-Type","application/json");
              headers.append("Authorization",this.token);
              headers.append("User",this.id);
			  console.log(this.id);
			  console.log('hello');
            return    this.http.delete(baseURL+"project/"+id+"/",{headers:headers}).map(res=>{
						console.log('nowwww');
                       console.log(res.json); 
                      })                  
                    .catch(error=>{
                      this.response=error;
                      var status =this.response.status;
                      console.log("resStatus ",status);
                      if(status == 400)
                      {
                        return Observable.throw("Invalid Access Please Login Again");
                         
                      }
                      else 
                      {
                        return this.httpProcessProvider.handleError(error);
                      }
                    }
                     );
  }
  getMyProjectById(id:number):Observable<Project> {
           var headers= new Headers();
           headers.append("Content-Type","application/json");
           headers.append("Authorization",this.token);
           headers.append("User",this.id);
     return  this.http.get(baseURL+"project/"+id+"/",{headers:headers})
                      .map(res=>{return this.httpProcessProvider.extractData(res);})
                     .catch(err=> {return this.httpProcessProvider.handleError(err);});
  }


 
}
