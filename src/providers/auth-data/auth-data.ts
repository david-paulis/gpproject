import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

/*
  Generated class for the AuthDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthDataProvider {
 private token:string;
 private id:string;
  constructor(public http: HttpClient) {
    console.log('Hello AuthDataProvider Provider');
  }
  setToken(token:string){
  this.token=token;
  }
  getToken():Observable<string>{
  return Observable.of(this.token) ;
  }
  setId(id:string){
   this.id=id;
  }
  getId(): Observable<string>{
   return Observable.of(this.id);
  }
}
