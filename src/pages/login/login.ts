import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { MainPage } from '../';
import {Users} from "../../providers/users/user"
import { Storage } from '@ionic/storage';
import { FormGroup ,FormBuilder, Validators } from '@angular/forms';
import {AuthDataProvider} from '../../providers/auth-data/auth-data';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
    loginForm:FormGroup
   errMsg:"";
   username="";
   password="";
  constructor(private userService:Users,public navCtrl: NavController,
    private storage: Storage,private authService:AuthDataProvider,
    public toastCtrl: ToastController, private formBuilder:FormBuilder ) {
      this.loginForm = this.formBuilder.group({
        username:["",Validators.required],
        password:["",Validators.required]
      });
   

      this.storage.get("username").then((username)=>{
          if(username)
          {
           this.username = username;
          }
      });
      this.storage.get("password").then((password)=>{
        if(password){
          this.password = password;
        }
      });

  }

  // Attempt to login in through our User service
  doLogin() {
   
  this.userService.login(this.loginForm.value).subscribe((resp)=>{
    this.authService.setId(resp.id);
    this.storage.set("user",resp);
    this.storage.set("id",resp.id);
    this.storage.set("username",resp.username);
    this.storage.set("password",resp.password);
    this.navCtrl.push(MainPage); 
  },
  (err)=>{
   //this.navCtrl.push(MainPage); 

    this.errMsg=err;
    //unable to log in
    let toast = this.toastCtrl.create({
      message:this.errMsg ,
      duration:3000,
      position:"top"
    });
    toast.present();
  });
  
  }
}
