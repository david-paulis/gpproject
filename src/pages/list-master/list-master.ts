import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';
import {ItemCreatePage} from '../../pages/item-create/item-create';
import { ProjectsProvider} from "../../providers/project/projects";
import {Project} from "../../shared/project";
@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  currentItems: Project[];
  errMsg:string="";
  constructor(private projectProvider:ProjectsProvider,public navCtrl: NavController, public modalCtrl: ModalController) {
     this.projectProvider.getAllProject().subscribe(res=>{
    this.currentItems =res;
    } , err =>{this.errMsg=err; });

  } 

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
    addItem() {
    let addModal = this.modalCtrl.create(ItemCreatePage);
    addModal.onDidDismiss(item => {
      if (item) {
       
       this.projectProvider.addProject(item).subscribe((project)=>{
        console.log("project ",project.project_name +" added succussfully .");
        //load projects
        this.currentItems=null;
        this.projectProvider.getAllProject().subscribe(res=>{
          this.currentItems =res;
          } , err =>{this.errMsg=err; });
       },err=>console.log(err));
        
      }
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item:Project) {
  
    this.projectProvider.removeProject(item.id).subscribe((res)=>{
      console.log("item deleted successfully ");
      this.projectProvider.getAllProject().subscribe(res=>{
        this.currentItems =res;
        } , err =>{this.errMsg=err; }); 
    },err=> console.log(err));
  
      
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Project) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }
}
