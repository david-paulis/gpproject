import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ProjectsProvider} from '../../providers/project/projects';
import {Project} from '../../shared/project';
import * as jQuery from 'jquery';
import * as _ from 'lodash';
import * as $ from 'backbone';
import  * as joint from '../../../node_modules/jointjs/dist/joint.js';
import {cls} from '../../shared/cls';
import {meth_class} from '../../shared/meth_class';
import {relation} from '../../shared/relation';


/**
* Generated class for the DrawPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */   
import { Attribute } from '@angular/compiler';
import {attr_class} from '../../shared/attr_class';
import { dia } from '../../../node_modules/jointjs/dist/joint.js';
import { concat } from 'rxjs/operator/concat';

@IonicPage()
@Component({
  selector: 'page-draw',
  templateUrl: 'draw.html',
  styles:["draw.scss", "../node_modules/jointjs/css/layout.css"]
})

export class DrawPage implements OnInit {

indexedArray: {[key: number]: number} []=[];
project:Project;
errMsg:"";

constructor(public navCtrl: NavController, public navParams: NavParams, private projectProvider:ProjectsProvider) {
    var id = navParams.get('id');


    this.projectProvider.getMyProjectById(id)
                        .subscribe(res =>
                        {    this.project =res; 
                             console.log("will draw this project ",this.project);
                             this.drawClassDiagram();
                        }    ,err=> this.errMsg=err);
                         
  } 
   
  ionViewDidLoad() {
    console.log('ionViewDidLoad DrawPage');
  }
  ngOnInit(){

} 
drawClassDiagram(){
  
  var uml = joint.shapes.uml;
  var graph = new joint.dia.Graph;
    var paper = new joint.dia.Paper({
     el: jQuery("#paper"),
     gridSize:1,
     height: jQuery("#paper").height(),
     width: jQuery("#paper").width(),
     model: graph,
    }); 
   var cls: cls;
   
   var my_relation=[];
   for( cls of this.project.cls){
	var att:string[]=[""];
    var meth:string[]=[""];
     cls.attr_class.forEach((attr)=>
     {
       att.push(attr.attr_name);
     });
     cls.meth_class.forEach((method)=>{
     meth.push(method.method_name+" ()");
     });
 


 graph.addCell(new uml.Class({
   position: { x:0  , y: 0 },
   size: { width: 180, height: 120 },
   name: cls.class_name,
   attributes:att ,
   methods: meth,
   id: cls.id,
   attrs: {
      '.uml-class-name-rect': {
          top: 2,
           fill: '#61549C',
           stroke: '#f6f6f6',
           'stroke-width': 1,
            rx: 4,
            ry: 4
      },
      '.uml-class-attrs-rect, .uml-class-methods-rect': {
          top: 2,
         fill: '#61549C',
           stroke: '#f6f6f6',
           'stroke-width': 1,
            rx: 4,
           ry: 4
      },
  '.uml-class-name-text': {
            ref: '.uml-class-name-rect',
            'ref-y': 0.5,
            'y-alignment': 'middle',
            fill: '#f6f6f6',
            'font-family': 'Roboto Condensed',
            'font-weight': 'Normal',
            'font-size': 12
      },
      '.uml-class-attrs-text': {
          ref: '.uml-class-attrs-rect',
            'ref-y': 0.5,
            'y-alignment': 'middle',
       fill: '#f6f6f6',
            'font-family': 'Roboto Condensed',
            'font-weight': 'Normal',
            'font-size': 12
      },
      '.uml-class-methods-text': {
          ref: '.uml-class-methods-rect',
            'ref-y': 0.5,
            'y-alignment': 'middle',
     fill: '#f6f6f6',
            'font-family': 'Roboto Condensed',
            'font-weight': 'Normal',
            'font-size': 12
      }

  }}));

  var rect = new joint.shapes.basic.Rect({
    position: { x: 100, y: 30 },
    size: { width: 100, height: 30 },
    attrs: { rect: { fill: 'blue' }, text: { text: 'my box', fill: 'white' } }
});
 //this.indexedArray.push({ [cls.id]:t.id });

   }//end for

//for relations
 for(cls of this.project.cls){
   for(var rel of cls.relation)
   {
  
     var source =rel.class_table1; //this.indexedArray.filter((item)=>{return item[rel.class_table1];})[0][rel.class_table1];
     var target= rel.class_table2;//this.indexedArray.filter((item)=>{return item[rel.class_table2];})[0][rel.class_table2];

    if(rel.relation_type == "inheritance"){
    var  cell=   new uml.Generalization( { source: { id: source }, target: { id: target }} );
      graph.addCell(cell);
    }else if(rel.relation_type =="association")
    {
     var  cell=   new uml.Association( { source: { id: source }, target: { id: target }} );

     graph.addCell(cell);

    }else if (rel.relation_type =="composition")
    {
     var  cell=   new uml.Composition( { source: { id: source }, target: { id: target }} );
     graph.addCell(cell);


    }else if (rel.relation_type=="aggregation"){
     var  cell=   new uml.Aggregation( { source: { id: source }, target: { id: target }} );
     graph.addCell(cell);

    }
   
     
   }
 }

 var graphBBox = joint.layout.DirectedGraph.layout(graph, {
    nodeSep: 50,
    edgeSep: 80,
    rankDir: "TB"
});
 
}



} 