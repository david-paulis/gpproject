import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DrawPage} from '../../pages/draw/draw';

import {ProjectsProvider} from '../../providers/project/projects';
import {Project} from '../../shared/project';
@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})
export class ItemDetailPage {
  item: Project;

  constructor(public navCtrl: NavController, navParams: NavParams) {
    this.item = navParams.get('item');
  }
  
draw(id:number){
  this.navCtrl.push(DrawPage, {
    id: id
  });
}
}
