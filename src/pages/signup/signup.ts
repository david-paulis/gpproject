import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import {Users} from '../../providers/users/user';
import {Validators , FormBuilder , FormGroup} from '@angular/forms';
import { MainPage } from '../';
import { Storage } from '@ionic/storage';
import {AuthDataProvider} from '../../providers/auth-data/auth-data';
@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {


  errMsg="";
  registerForm: FormGroup;
  constructor(public navCtrl: NavController,private storage:Storage,
    public toastCtrl: ToastController,private usersProvider:Users 
      ,private formBuilder:FormBuilder,private authService:AuthDataProvider) {
   this.registerForm = this.formBuilder.group({
      first_name:['',[Validators.required]],
      last_name:['',[Validators.required]],
      phone:['',[Validators.required,Validators.pattern] ],
      email:['',[Validators.required,Validators.email] ],
      username:['',[Validators.required,Validators.minLength(3)] ],
      password:['',[Validators.required,Validators.minLength(3)] ],
      job_title:['',Validators.required]

   });
  }

  doSignup() {
    // Attempt to login in through our User service
    console.log(this.registerForm.value)
    this.usersProvider.signup(this.registerForm.value).subscribe((resp) => {
      this.authService.setId(resp.id);
      this.storage.set("user",resp);
      this.storage.set("id",resp.id);
      this.storage.set("username",resp.username);
      this.storage.set("password",resp.password);
      this.navCtrl.push(MainPage);
    }, (err) => {
       
      //this.navCtrl.push(MainPage);
      this.errMsg=<any>err;
      // Unable to sign up
      let toast = this.toastCtrl.create({
        message: this.errMsg,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
}
