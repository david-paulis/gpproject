import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-cards',
  templateUrl: 'cards.html'
})
export class CardsPage {
  item: string;

  constructor(public navCtrl: NavController ,private storage:Storage) {
     this.storage.get("user").then(user =>{
       this.item=user;
       console.log(this.item);
    });
       
          
   

  }
}
