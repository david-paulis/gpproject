
import {cls} from './cls';
export interface Project{
 id:number;
 project_name:string;
 short_description:string;
 description:string;
 cls:cls[];

}