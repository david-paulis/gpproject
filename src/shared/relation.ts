export interface relation{
    id:number;
    class_table1:number;
    class_table2:number;
    relation_type:string;
    relation_label:string;
}