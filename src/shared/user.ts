export interface User {
    id:string,
    username:string;
    password:string;
    first_name:string;
    last_name:string;
    job_title:string;
    phone:string;
    email:string;
    img:string;
}