import {relation} from "./relation";
import {attr_class}  from './attr_class';
import {meth_class} from './meth_class';

export interface cls{
    id:number;
    class_name:string;
    attr_class:attr_class[];
    meth_class:meth_class[];
    relation: relation[];
}